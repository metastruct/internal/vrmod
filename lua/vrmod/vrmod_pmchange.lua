--[[

	this is a shitty workaround for some addons breaking player:GetModel() on the client
	
	for example, clear a pac3 outfit that changed your player model, then change your pm normally using the default gmod player model selector
	and player:GetModel() will always return the old model no matter how many times you change pm

--]]
if CLIENT then
	local pminfo = {}

	net.Receive("vrmod_pmnet", function()
		local plyid = net.ReadUInt(24)
		local model = net.ReadString()
		pminfo[plyid] = model
		local ply = Player(plyid)

		if IsValid(ply) then
			local old = ply.vrmod_pm
			ply.vrmod_pm = model
			hook.Run("VRModPlayerModelChanged", ply, model, old)
		end
	end)

	hook.Add("NotifyShouldTransmit", "vrmod_pmnet", function(ply, should)
		if not should then return end
		if not ply:IsPlayer() then return end
		local plyid = ply:UserID()
		local new = pminfo[plyid]

		if ply.vrmod_pm ~= new then
			local old = ply.vrmod_pm
			ply.vrmod_pm = new
			hook.Run("VRModPlayerModelChanged", ply, new, old)
		end
	end)

	return
end

util.AddNetworkString("vrmod_pmnet")
local Entity = FindMetaTable"Entity"
local Entity_SetModel = Entity.SetModel
local wantSend = _G.vrmod_pmnet_wantSend or {}

local sent = setmetatable({}, {
	__mode = 'k'
})

local init

if _G.vrmod_pmnet_wantSend then
	init = false
	print("Reloading vrmod_pmnet")
else
	_G.vrmod_pmnet_wantSend = wantSend

	init = function()
		init = false

		function Entity:SetModel(mdl, ...)
			if self:IsPlayer() then
				wantSend[self] = mdl or false
			end

			return Entity_SetModel(self, mdl, ...)
		end
	end
end

hook.Add("Tick", "vrmod_pmnet", function()
	for ply, mdl in pairs(wantSend) do
		local old = sent[ply]

		if mdl ~= old then
			sent[ply] = mdl
			wantSend[ply] = nil

			if ply:IsValid() then
				net.Start("vrmod_pmnet")
				net.WriteUInt(ply:UserID(), 24)
				net.WriteString(mdl or "")
				net.Broadcast()
			else
				sent[ply] = nil
			end
		else
			wantSend[ply] = nil
		end
	end

	if init then
		init()
	end
end)